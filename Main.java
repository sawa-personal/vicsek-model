//*******************************************************
// Main.java
// created by Sawada Tatsuki on 2018/01/11.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* Processingによる描画処理 */

import processing.core.*;

public class Main extends PAppletWrapper {

    public void settings() {
		size(windowSize, windowSize); //ウィンドウサイズを指定
		setTerminate(-1); //終了ステップ数を設定
		doesSave(false); //動画を保存
		setRegulation(windowSize / simulator.interval, new Vector(windowSize / 2.0, windowSize / 2.0)); //拡大比と原点位置を設定
    }

	public void setup() {
		strokeWeight(1.0f); //線の太さを指定
		stroke(64); //枠線の色
		noStroke(); //点の枠線なし
    }

	//描画処理
    public void controller() {
		//frameRate(10); //描画レート
		//overlook(); //俯瞰
		
		t = simulator.getStep(); //経過ステップ数を取得
		background(255);//背景色を指定．画面をリセットする役割もある
		
		drawAgents(); //エージェントを描画
		
		//showCountLabel(t); //カウントラベルを表示
    }
	
	//エージェントを描画
	public void drawAgents() {		
		Vector[] position = getPosition(simulator.agent); //座標を取得
		int num = Agent.num; //エージェント数

		int d = 8; //粒子の直径
	
		for(int i = 0; i < num; i++) {
			fill(64); //色を設定
			ellipse((float)position[i].getX(), (float)position[i].getY(), d, d); //プロット
		}
	}
	
    public static void main(String args[]) {
		PApplet.main("Main"); //Mainクラスを呼び出してProcessingを起動
    }
}
