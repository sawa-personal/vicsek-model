//*******************************************************
// Agent.java
// created by Sawada Tatsuki on 2018/05/21.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* エージェントのオブジェクト */

import java.util.*;

public class Agent extends AbstractAgent {
	public static int num = 300; //エージェントの総数
	private double R = 100; //認識範囲の半径
	
	//コンストラクタ
	public Agent(int id, Simulator simulator) {
		super(id, simulator); //コンストラクタを継承
		setCognizance(R, 0); //認識範囲の半径と死角を初期化
		setVMax(2); //最大速度を設定
	}
	
	private Set<AbstractAgent> neighbors; //認識範囲内のエージェント集合
	//観測フェーズ	
	protected void observe() {
		neighbors = getNeighbors(simulator.agent); //認識可能な近傍を取得
	}
	
	//パラメータ
	private double eta = 0.2; //周囲の個体と向きを揃えるときに (個体が行動を選択す過程で) 犯す誤差の大きさ
	
	//行動フェーズ
	public void act() {
		Vector vSum = new Vector(0, 0); //近傍の速度の合計
		Iterator<AbstractAgent> iter = neighbors.iterator(); //セットのイテレータ
		while(iter.hasNext()) { //要素を順に読み取る
			AbstractAgent neighbor = iter.next(); //近傍エージェント
			vSum = vSum.add(neighbor.getVelocity()); //合計速度に加算
		}

		/***** start: Vicsek モデルのノイズ付加法 *****/
		double arg = Vector.argument(vSum); //合計ベクトルの偏角を算出
		
		double whiteNoise = 2 * Math.PI * rnd.nextDouble() - Math.PI; //[-π, π]のホワイトノイズ
		v = Vector.foward(arg + eta * whiteNoise); //速度を更新

		/***** start: Gregoire and Chate モデルのノイズ付加法 *****/
		/***** end: Gregoire and Chate モデルのノイズ付加法 *****/

		v = v.scale(vMax); //最大速度にする
		p = p.add(v); //位置の更新
		torus();
	}

	//秩序変数
	public double orderParameter() {
		return 0;
	}}
