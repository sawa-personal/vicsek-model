//*******************************************************
// PObject.java
// created by Sawada Tatsuki on 2018/03/10.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* Processingで描画するオブジェクト */

import java.util.*;
import processing.core.*;

public abstract class PObject extends PApplet {
    protected PApplet pa; //呼び出し元のPApplet
    public abstract void draw(); //描画処理 (オーバーライド前提)
}
