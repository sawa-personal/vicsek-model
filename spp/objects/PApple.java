//*******************************************************
// PApple.java
// created by Sawada Tatsuki on 2018/03/10.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* りんごマークのオブジェクト */

import processing.core.*;

public class PApple extends PObject {
    private Vector position; //位置
    private Vector velocity; //方向
    private double radius; //半径
    private PColor color; //色
    private PColor borderColor; //枠線の色
    private Vector edge = new Vector(0, 0, 0); //ヘタの先端座標
    
    //コンストラクタ
    public PApple(PApplet pa, Vector position, Vector velocity, double radius) {
		this.pa = pa; //呼び出し元のPApplet
		setPosition(position); //位置を設定
		setVelocity(velocity); //姿勢を設定
		setRadius(radius); //半径を設定
		this.color = new PColor(255); //中身の色:デフォルト白
		this.borderColor = new PColor(0); //枠線の色:デフォルト黒
    }

    //位置を設定
    public void setPosition(Vector position) {
		this.position = position;
    }

    //方向を設定
    public void setVelocity(Vector velocity) {
		this.velocity = velocity;
    }
    
    //半径を設定
    public void setRadius(double radius) {
		if(radius < 0) {
			System.exit(1); //半径が負の値で強制終了
		}
		this.radius = radius;
    }

    //ヘタの先端座標を求める
    private void setEdge() {
		Vector dir = velocity.clone();
		dir = dir.normalize();
		dir = dir.scalarMultiply(radius * 1.2);
		dir = position.add(dir);
		this.edge.set(dir);
    }
    
    //中身の色を設定
    public void setColor(PColor c) {
		color = c;
    }

    //枠線の色を設定
    public void setBorderColor(PColor c) {
		borderColor = c;
    }

    //オブジェクトを描画
    public void draw() {
		pa.fill((float)color.r, (float)color.g, (float)color.b, (float)color.a); //色を設定
		pa.ellipse((float)position.getX(), (float)position.getY(), 2 * (float)radius, 2 * (float)radius); //実の描画
		setEdge(); //ヘタの先端座標を求める
		pa.line((float)position.getX(), (float)position.getY(), (float)edge.getX(), (float)edge.getY()); //ヘタの描画
    }
}
