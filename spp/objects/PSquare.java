//*******************************************************
// PSquare.java
// created by Sawada Tatsuki on 2018/03/10.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 四角形のオブジェクト */

import processing.core.*;

public class PSquare extends PObject {
    public Vector position; //位置
    private double size; //四角の大きさ
    private PColor color; //色
    private PColor borderColor; //枠線の色
    
    //コンストラクタ
    public PSquare(PApplet pa, Vector position, double size) {
	this.pa = pa; //呼び出し元のPApplet
	setPosition(position); //位置を設定
	setSize(size); //半径を設定
	this.color = new PColor(255); //中身の色:デフォルト白
	this.borderColor = new PColor(0); //枠線の色:デフォルト黒
    }

    //位置を設定
    public void setPosition(Vector position) {
	this.position = position;
    }
    
    //半径を設定
    public void setSize(double size) {
	if(size < 0) {
	    System.exit(1); //半径が負の値で強制終了
	}
	this.size = size;
    }
    
    //中身の色を設定
    public void setColor(PColor c) {
	color = c;
    }

    //枠線の色を設定
    public void setBorderColor(PColor c) {
	borderColor = c;
    }

    //オブジェクトを描画
    public void draw() {
	pa.fill((float)color.r, (float)color.g, (float)color.b, (float)color.a); //色を設定
	pa.rect((float)(position.getX() - size / 2), (float)(position.getY() - size / 2), (float)size, (float)(size/1.4)); //四角の描画
    }
}
