//*******************************************************
// PAppletWrapper.java
// created by Sawada Tatsuki on 2018/05/25.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* PAppletのラッパークラス */

import java.util.*;
import java.text.*;
import java.io.*;
import processing.core.*;

public abstract class PAppletWrapper extends PApplet {
	protected static Simulator simulator; //シミュレータのインスタンス
	public double scale; //描画スケール
	private Vector shift; //原点の表示位置

	protected int windowSize = 600; //ウィンドウサイズ
	private int terminate = -1; //終了ステップ数．-1 のとき無限ループ
	public int t; //経過ステップ数
	
    //コンストラクタ
    public PAppletWrapper() {
		windowSize = 600; //ウィンドウサイズ
		simulator = new Simulator(); //シミュレータを生成
		setRegulation(1, new Vector(0, 0)); //ベクトルを補正．拡大比と平衡移動量
    }

	public abstract void controller(); //描画処理: オーバーライド前提
	
	public void draw(){
		simulator.run(); //系を動かす
		
		controller(); //描画処理: オーバーライド前提
		
		if(doesSave) {
			snap(); //スクリーンショットを保存
		}
		
		//終了条件
		if(t == terminate) {
			if(doesSave) {
				saveMovie(); //動画を保存
			}
			noLoop(); //描画停止
		}
	}

	/********** start: 終了条件の設定 **********/
	protected void setTerminate(int terminate) {
		this.terminate = terminate;
	}
	/********** end: 終了条件の設定 **********/
	
	/********** start: 日付の設定 **********/
	private String getDate() {
		Date date = new Date();  //Dateオブジェクトを生成
		SimpleDateFormat sdf = new SimpleDateFormat("YYMMdd'_'HHmmss"); //SimpleDateFormatオブジェクトを生成
		return sdf.format(date);
	}

	protected String date = getDate(); //シミュレータ実行時点の日時
	/********** end: 日付の設定 **********/
	
	/********** start: カウントラベルの表示処理 **********/	
	protected void showCountLabel(int t) {
		textSize(24); //文字サイズを指定
		fill(64); //文字色を指定
		String countLabel = "t = " + Integer.toString(t);
		text(countLabel, (float)0.04* windowSize, (float)0.064 * windowSize);
	}
	/********** end: カウントラベルの表示処理 **********/
	
	/********** start: 画像・動画の保存処理 **********/
	private boolean doesSave = false; //動画を保存するか決めるフラグ
	protected String dirPass = "frames/" + date + "/"; //スナップ保存先ディレクトリ
	
	//動画を保存するか否か
	protected void doesSave(boolean doesSave) {
		this.doesSave = doesSave;
	}

	//スクリーンショットを保存
	protected void snap() {
		saveFrame("frames/" + date + "/" + "######.png");//スクリーンショットを保存
	}

	//動画を自動生成
	protected void saveMovie() {
		try {
			Runtime rt = Runtime.getRuntime();
			String command = "ffmpeg -r 30 -start_number 0 -i " + dirPass + "%6d.png " + "-vframes " + 2 * (terminate + 1) + " -crf 1  -vcodec libx264 -pix_fmt yuv420p -r 60 " + dirPass + "/../" + date + ".mp4";
			rt.exec(command); //コマンド実行
			System.exit(0); //プログラム終了
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	/********** end: 画像・動画の保存処理 **********/

	//座標の補正 (拡大比率と原点表示位置) を設定
	protected void setRegulation(double scale, Vector shift) {
		this.scale = scale;
		this.shift = shift;
	}
	
	//ベクトルを画面サイズに合わせてスケーリング・平行移動
	private Vector regulateVector(Vector p) {
		Vector q = p.clone();
		q = q.scalarMultiply(scale); //スケーリング
		q.set(q.getX(), -q.getY()); //y軸反転(processingは画像処理座標系であるため)
		q = q.parallel(shift.scalarMultiply(-scale)); //平行移動
		return q;
	}
	
	//俯瞰
	protected void overlook() {
		scale(0.5f, 0.5f); //拡大
		translate(windowSize / 2, windowSize / 2); //座標移動
	}

	

	
	//エージェントの座標を取得
	protected Vector[] getPosition(AbstractAgent[] agent) {
		int num = agent.length;
		Vector position[] = new Vector[num]; //座標
		for(int i = 0; i < num; i++){
			position[i] = agent[i].getPosition(); //座標のインスタンス
			position[i] = regulateVector(position[i]); //表示用に補正
		}
		return position;
	}

	
	//エージェントの速度を取得
	protected Vector[] getVelocity(AbstractAgent[] agent) {
		int num = agent.length;
		Vector velocity[] = new Vector[num]; //座標
		for(int i = 0; i < num; i++){
			velocity[i] = agent[i].getVelocity(); //座標のインスタンス
			velocity[i] = regulateVector(velocity[i]); //表示用に補正
		}
		return velocity;
	}
}
