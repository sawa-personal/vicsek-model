//*******************************************************
// AbstractAgent.java
// created by Sawada Tatsuki on 2018/05/21.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* エージェントの抽象クラス */

//package com.naked-hermit.www.spp
import java.util.*;

public abstract class AbstractAgent {
	public Vector p; //座標
    public Vector v; //速度
	public double vMax; //最大速度
    private double range; //認識範囲の半径
    private double blind; //死角
	private double blindCos; //視野角に対する余弦
		
    public int id; //エージェントのID
    public Simulator simulator; //属するシミュレータ
	
    protected static Random rnd = new Random(); //乱数生成用インスタンス

	private double width; //探索空間の横幅
	private double height; //探索空間の縦幅

    //コンストラクタ
    public AbstractAgent(int id, Simulator simulator) {
		this.id = id; //IDを設定
		this.simulator = simulator; //属するシミュレータを設定
		
		p = new Vector(0, 0); //座標
		v = new Vector(0, 0); //速度
		setCognizance(100, 0); //認識範囲の半径と死角を初期化
		vMax = 5; //最大速度の初期化
		width = simulator.interval; //探索空間の横幅
		height = simulator.interval; //探索空間の縦幅
    }
	
    //座標を設定
    public void setPosition(double x, double y) {
		this.p.set(x, y);
    }
	
    //座標を取得
    public Vector getPosition() {
		return p.clone();
    }

    //速度を設定
    public void setVelocity(double x, double y) {
		this.v.set(x, y);	
    }
	
    //速度を取得
    public Vector getVelocity() {
		return v.clone();
    }

	//最大速度を設定
	public void setVMax(double vMax) {
		this.vMax = vMax;
	}

	//認識 (認識半径と死角) に関する設定
	public void setCognizance(double range, double blind) {
		//例外処理
		if(range < 0 || blind < 0 || blind > 2 * Math.PI) {
			System.exit(1);
		}
		
		this.range = range; //認識範囲を設定
		this.blind = blind; //死角を設定
		this.blindCos = Math.cos(blind / 2); //対応する余弦を設定
	}
	
    //指定のエージェントが認識範囲内か
    public boolean cognize(AbstractAgent opp) {
		Vector toOpp = opp.p.subtract(p);	//自分から相手に向かうベクトル
		double cos = Vector.inner(v, toOpp) / (Vector.norm(v) * Vector.norm(toOpp)); //余弦
		return (-blindCos <= cos && cos <= 1); //認識範囲内 => true, 認識範囲外 => false
    }

	//認識範囲内のエージェント集合を返す
	public Set<AbstractAgent> getNeighbors(AbstractAgent[] agent) {
		int num = agent.length; //エージェントの数
		Set<AbstractAgent> neighbors = new HashSet<AbstractAgent>(); //認識範囲内のエージェント集合を初期化
		//認識可能なエージェント
		for(int j = 0; j < num; j++) {
			if(cognize(agent[j]) && j != id) { //他エージェントjが認識可能なとき
				neighbors.add(agent[j]); //近傍に追加
			}
		}
		return neighbors;
	}
	
	//最大の速さを超えていた場合，締め付ける
	protected void constrictVelocity() {
		if(v.norm() > vMax) {
			v = v.scale(vMax);
		}
	}
	
	//相手から自分に向かうベクトルを求める
	public Vector[][] calcToOwn(AbstractAgent[] agent) {
		int num = agent.length; //エージェントの数
		Vector[][] vectors = new Vector[num][num]; //jからiに向かうベクトルの集合
		
		for(int i = 0; i < num; i++) {
			for(int j = i + 1; j < num; j++) {		
				vectors[i][j] = (agent[i].p).subtract(agent[j].p); //jからiへのベクトル
				vectors[j][i] = vectors[i][j].scalarMultiply(-1); //逆向き
			}
		}
		return vectors;
	}
	
	//回りのエージェントとの距離を求める
	public double[] calcDistance(AbstractAgent[] agent) {
		int num = agent.length; //エージェントの数
		double[] distance = new double[num]; //求めるエージェント間距離
		
		for(int j = 0; j < num; j++) {
			Vector toOwn = p.subtract(agent[j].p); //jからiへのベクトル
			distance[j] = toOwn.norm(); //その長さ
		}

		return distance;
	}
	
	//2種類のエージェント間の距離を求める
	public double[][] calcDistancesBetweenTheTwo(AbstractAgent[] agentA, AbstractAgent[] agentB) {
		int numA = agentA.length; //エージェントaの数
		int numB = agentB.length; //エージェントbの数
		double[][] distanceBetweenTheTwo = new double[numA][numB]; //i番目のエージェントAと，j番目のエージェントBの間の距離
		
		for(int i = 0; i < numA; i++) {
			for(int j = 0; j < numB; j++) {
				Vector bToA = (agentA[i].p).subtract(agentB[j].p); //j(エージェントB)からi(エージェントA)へのベクトル
				distanceBetweenTheTwo[i][j] = bToA.norm(); //その長さ
			}
		}

		return distanceBetweenTheTwo;
	}
	
	//壁で跳ね返す
	protected void walled(){
		if(p.getX() > width) {
			p.setX(width); //位置を領域の端に
			v.setX(-v.getX()); //速度を逆方向に
		} else if(p.getX() < 0) {
			p.setX(0); //位置を領域の端に
			v.setX(-v.getX()); //速度を逆方向に
		}
		if(p.getY() > height) {
			p.setY(height); //位置を領域の端に
			v.setY(-v.getY()); //速度を逆方向に
		} else if(p.getY() < 0) {
			p.setY(0); //位置を領域の端に
			v.setY(-v.getY()); //速度を逆方向に		
		}	
	}

	//2次元トーラス
	protected void torus(){
		double agentSize = 20; //エージェントの大きさ
		if(p.getX() > width + agentSize) {
			p.setX(-agentSize); //位置を領域の端に
		} else if(p.getX() < -agentSize) {
			p.setX(width + agentSize); //位置を領域の端に
		}
		if(p.getY() > height + agentSize) {
			p.setY(-agentSize); //位置を領域の端に
		} else if(p.getY() < -agentSize) {
			p.setY(height + agentSize); //位置を領域の端に
		}	
	}
	
	//結合力を求める
	protected Vector getCoh(Set<AbstractAgent> agents) {
		Iterator<AbstractAgent> iter = agents.iterator(); //集合のイテレータ
		ArrayList<Vector> positionSet = new ArrayList<Vector>(); //座標リスト
		while(iter.hasNext()) { //要素を順に読み取る
			AbstractAgent agent = iter.next(); //着目エージェント
			positionSet.add(agent.getPosition()); //座標リストに追加
		}
		
		Vector coh = Vector.average(positionSet); //平均座標
		coh = coh.subtract(p); //平均座標に向かうベクトル
		return coh;
	}
	
	//整列力を求める	
	protected Vector getAlg(Set<AbstractAgent> agents) {
		Iterator<AbstractAgent> iter = agents.iterator(); //集合のイテレータ
		ArrayList<Vector> velocitySet = new ArrayList<Vector>(); //速度リスト
		while(iter.hasNext()) { //要素を順に読み取る
			AbstractAgent agent = iter.next(); //着目エージェント
			velocitySet.add(agent.getVelocity()); //速度リストに追加
		}
		
		Vector alg = Vector.average(velocitySet); //平均速度
		alg = alg.subtract(p); //平均座標に向かうベクトル
		return alg;
	}
	
	//分離力を求める
	protected Vector getSep(Set<AbstractAgent> agents) {
		Iterator<AbstractAgent> iter = agents.iterator(); //集合のイテレータ
		ArrayList<Vector> velocitySet = new ArrayList<Vector>(); //座標リスト

		Vector sep = new Vector(0, 0); //受ける総合力
		
		while(iter.hasNext()) { //要素を順に読み取る
			AbstractAgent agent = iter.next(); //着目エージェント
			Vector toOwn = p.subtract(agent.p); //相手から自分に向かうベクトル
			double distanceSq = Vector.distanceSq(new Vector(0, 0), toOwn); //二乗ノルム
			toOwn = toOwn.scalarDivide(distanceSq); //二乗ノルムで割る．(距離で割る=斥力を単位ベクトル，さらに距離で割る=斥力は距離に反比例)
			sep = sep.add(toOwn); //総合力に加算
		}
		
		return sep;
	}
	
	//末尾エージェントの情報を表示
	public void printInfo() {
		System.out.printf("p = (%.2f, %.2f), v = (%.2f, %.2f), f(p) = %.2f\n",
						  p.getX(), p.getY(), v.getX(), v.getY());
	}
}
