//*******************************************************
// Vector.java
// created by Sawada Tatsuki on 2018/05/21.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* ベクトルのオブジェクト */

import java.util.*;

public class Vector {
    private double x;
    private double y;
	private double z;
	
    //コンストラクタ
    public Vector(double x, double y, double z){
		set(x, y, z);
    }
	
    //コンストラクタ (2D)
    public Vector(double x, double y){
		set(x, y, 0);
    }	
	
    //座標を設定
    public void set(double x, double y, double z){
		this.x = x;
		this.y = y;
		this.z = z;
    }

    //座標を設定 (2D)
    public void set(double x, double y){
		set(x, y, 0);
    }
	
    //座標を設定
    public void set(Vector p){
		this.x = p.getX();
		this.y = p.getY();
		this.z = p.getZ();
    }

    //xを設定
    public void setX(double x) {
		this.x = x;
    }
    
    //yを設定
    public void setY(double y) {
		this.y = y;
    }
	
    //zを設定
    public void setZ(double z) {
		this.z = z;
    }
	
    //xを取得
    public double getX(){
		return x;
    }
    
    //yを取得    
    public double getY(){
		return y;
    }

    //zを取得    
    public double getZ(){
		return z;
    }

	//平行移動
	public Vector parallel(Vector dp) {
		return new Vector(x - dp.getX(), y - dp.getY(), z - dp.getZ());
	}

	//平行移動 (座標指定，2D)
	public Vector parallel(double dx, double dy) {
		return new Vector(x - dx, y - dy, z);
	}
	
    //和
    public Vector add(Vector p) {
		return new Vector(x + p.getX(), y + p.getY(), z + p.getZ());
    }

    //和 (座標指定)
    public Vector add(double x, double y, double z) {
		return new Vector(this.x + x, this.y + y, this.z + z);
    }

	//和 (2D)
    public Vector add(double x, double y) {
		return add(x, y, 0);
    }
	
    //差
    public Vector subtract(Vector p) {
		return new Vector(x - p.getX(), y - p.getY(), z - p.getZ());
    }

    //2点間の二乗距離
    public double distanceSq(Vector p) {
		double X = (x - p.getX()); //距離のx成分
		double Y = (y - p.getY()); //距離のy成分
		double Z = (z - p.getZ()); //距離のz成分
		return X * X + Y * Y + Z * Z;
    }
	
    //2点間の二乗距離 (static)
    public static double distanceSq(Vector p, Vector q) {
		double X = (p.getX() - q.getX()); //距離のx成分
		double Y = (p.getY() - q.getY()); //距離のy成分
		double Z = (p.getZ() - q.getZ()); //距離のz成分
		return X * X + Y * Y + Z * Z;
    }

    //2点間の距離
    public double distance(Vector p) {
		return Math.sqrt(distanceSq(this, p)); //二乗距離のルートを返す
    }
	
    //2点間の距離 (static)
    public static double distance(Vector p, Vector q) {
		return Math.sqrt(distanceSq(p, q)); //二乗距離のルートを返す
    }
	
    //ノルム
    public double norm() {
		Vector zero = new Vector(0, 0);
		return distance(zero, this);
    }

    //ノルム (static)
    public static double norm(Vector p) {
		Vector zero = new Vector(0, 0);
		return distance(zero, p);
    }
    
    //内積 (static)
    public static double inner(Vector p, Vector q) {
		return p.getX() * q.getX() + p.getY() * q.getY() + p.getZ() * q.getZ();
    }

    //なす角の余弦 (static)
    public static double cos(Vector p, Vector q) {
		return inner(p, q) / (norm(p) * norm(q));
    }
    
    //なす角
    public static double angle(Vector p, Vector q) {
		double cos = cos(p, q); //2ベクトルの余弦
		double angle = Math.acos(cos); //なす角(鋭角)
		return angle;
    }

    //偏角 (極座標の始線から反時計周りに測った角度)
	public static double argument(Vector p) {
		Vector uX = new Vector(1, 0); //水平単位ベクトル
		p = p.normalize(); //pを正規化
		double argument = Vector.angle(uX, p); //なす角を求める		
		if(p.getY() < 0) {
			argument += 2 * Math.PI;
		}
		return argument;
    }


	
    //スカラー倍
    public Vector scalarMultiply(double k) {
		return new Vector(k * x, k * y, k * z);
    }
    
    //スカラー倍 (static)
    public static Vector scalarMultiply(Vector p, double k) {		
		return new Vector(k * p.getX(), k * p.getY(), k * p.getZ());
    }

	//スカラー割り
    public Vector scalarDivide(double k) {
		//ゼロ割りで強制終了
		if(k == 0) {
			System.exit(1);
		}
		return new Vector(x / k, y / k, z / k);
    }
    
    //スカラー割り (static)
    public static Vector scalarDivide(Vector p, double k) {
		//ゼロ割りで強制終了
		if(k == 0) {
			System.exit(1);
		}
		return new Vector(p.getX() / k, p.getY() / k, p.getZ() / k);
    }
	
    //正規化
    public Vector normalize(){
		Vector q = new Vector(0, 0, 0); //返すベクトル
		double norm = norm(this); //自分の長さ
		if(norm != 0) {
			q = scalarMultiply(1.0 / norm);
		}
		return q;
    }

    //正規化 (static)
    public static Vector normalize(Vector a){
		Vector q = new Vector(0, 0, 0); //返すベクトル
		double norm = norm(a); //aの長さ
		if(norm != 0) {
			q = scalarMultiply(a, 1.0 / norm);
		}
		return q;
    }
	
	//ベクトルの長さを設定
	public Vector scale(double k) {
		Vector q = new Vector(0, 0, 0); //返すベクトル
		double norm = norm(this); //自分の長さ
		if(norm != 0) {
			q = new Vector(k * x / norm, k * y / norm, k * z / norm);
		}
		return q;
	}
	
	//z軸周りの回転
	public Vector roundZ(double deg) {
		double cos = Math.cos(deg);
		double sin = Math.cos(deg);
		return new Vector(cos * x - sin * y, sin * x + cos * y, z);
	}

	//指定した角度を指す2次元単位ベクトル
	public static Vector foward(double rad) {
		return new Vector(Math.cos(rad), Math.sin(rad));
	}
	
	//リストで与えられたベクトルの平均を算出
	public static Vector average(ArrayList<Vector> vectors) {
		int num = vectors.size(); //集合に格納されたエージェントの数
		Vector average = new Vector(0, 0); //求める平均

		Iterator<Vector> iter = vectors.iterator(); //集合のイテレータ
		while(iter.hasNext()) { //要素を順に読み取る
			Vector vector = iter.next();
			average = average.add(vector); //重心ベクトルに加算
		}

		if(num != 0) {
			average = average.scalarDivide(num); //総数で割る
		}
		
		return average;
	}
	
    //コピー
    public Vector clone(){
		return new Vector(x, y, z);
    }
}
