//*******************************************************
// Simulator.java
// created by Sawada Tatsuki on 2018/05/21.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* シミュレータの抽象クラス */

import java.util.*;

public abstract class AbstractSimulator {
	private int t; //経過ステップ数
	private double width = 600; //空間の横幅(px)
	private double height = 600; //空間の縦幅(px)

	protected static Random rnd = new Random(); //乱数生成用インスタンス
	
    //コンストラクタ
    public AbstractSimulator() {
		t = 0; //ステップ数を初期化
    }
	
    ///シミュレータを1ステップ動かす
    public void run(){
		process(); //処理を実行
		t++; //ステップ数をカウント
    }
	
    abstract protected void process();	//シミュレータの処理内容: オーバーライド前提
	
	//現在のステップを求める
	public int getStep() {
		return t;
	}

	//空間の大きさを設定
	protected void size(double width, double height) {
		this.width = width;
		this.height = height;
	}

}
