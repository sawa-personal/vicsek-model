//*******************************************************
// Simulator.java
// created by Sawada Tatsuki on 2018/05/21.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* シミュレータのクラス */

public class Simulator extends AbstractSimulator {
    public Agent agent[]; //エージェント
	
    public static int num = Agent.num; //ネットワークのノード数	
	public static int interval = 600; //空間の1辺の長さ
	
    public Simulator() {
		size(interval, interval); //空間の大きさを設定
		
		/* start: エージェントの初期設定 */
		agent = new Agent[num];
		
		for(int i = 0; i < num; i++) {
			agent[i] = new Agent(i, this); //ノードのインスタンス生成
			agent[i].setPosition(interval * (rnd.nextDouble() - 0.5), interval * (rnd.nextDouble() - 0.5)); //ランダムな座標
			double r = Math.random() * 2.0 * Math.PI; //ランダムな角度
			agent[i].setVelocity(3 * Math.random() * Math.cos(r), 3 * Math.random() * Math.sin(r)); //大きさ一定，方向ランダムな速度
		}
		/* end: エージェントの初期設定 */
		
		//観測フェーズ
		for(int i = 0; i < num; i++) {
			agent[i].observe();
		}
    }

	//処理の内容
    public void process() {
		//行動フェーズ
		for(int i = 0; i < num; i++) {
			agent[i].act();
		}

		//観測フェーズ
		for(int i = 0; i < num; i++) {
			agent[i].observe();
		}
    }

	//エージェント情報を取得
	public Agent[] getAgents() {
		return agent;
	}
}
